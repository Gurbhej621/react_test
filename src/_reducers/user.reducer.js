const initialState = { anchor: 'left',
    user: [],
    open: false,
    _id: '',  
    name: '',
    email: '',
    phone: '',
    gender: ''
 };


export function user(state = initialState, action) {
    switch (action.type) {
        case 'FETECHED_ALL_USER':
            return {
            ...state,
            user: action.user
            };
        case 'USER_DETAIL':
            return {
                ...state,
                _id: action._id,  
                name: action.name,
                email: action.email,
                phone: action.phone,
                gender: action.gender
            };
        case "USER_UPDATED":
            return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };    
        default:
            return state
    }
  }