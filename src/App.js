import React, { Component } from 'react';
import './App.css';
import { Router, Switch, Route} from 'react-router-dom';
import { User } from './users/user.component';
import { AddUser } from './users/adduser.component';
import { Home } from './home/';
import { history } from './_helpers';
import { PrivateRoute } from './_components';


class App extends Component {
  render() {
    return (
      <div className="App">
        <Router history={history}>
          <div>            
              <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/user' component={User} />
                <Route exact path='/add-user' component={AddUser} />
                <Route exact path='/edit-user/:id' component={AddUser} />
              </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
