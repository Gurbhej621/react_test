import { userService } from '../_services';
import { history } from '../_helpers';

export const userAction = {
    getUser,
    getUserById,
    onChangeProps,
    editUserInfo,
    createUser,
    deleteUserById
};

function getUser(){
    return dispatch => {
        let apiEndpoint = 'api/users';
        userService.get(apiEndpoint)
        .then((response)=>{
            console.log(response);
            dispatch(changeVendorsList(response.data));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}

function createUser(payload){
    return dispatch => {
        let apiEndpoint = 'api/users';
        userService.post(apiEndpoint, payload)
        .then((response)=>{
            dispatch(createUserInfo());
            history.push('/user');
        }) 
    }
}

function getUserById(id){

    return dispatch => {
        let apiEndpoint = 'api/users/'+ id;
        userService.get(apiEndpoint)
        .then((response)=>{
            let data = response.data? response.data: [];
            dispatch(editUserDetails(data));
        })
    };
}

function onChangeProps(props, event){
    return dispatch =>{
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}

function editUserInfo(id, payload){
    return dispatch => {
        let apiEndpoint = 'api/users/'+ id;
        userService.put(apiEndpoint, payload)
        .then((response)=>{
            dispatch(updatedUserInfo());
            history.push('/user');
        }) 
    }
}

function deleteUserById(id){
    return dispatch => {
        let apiEndpoint = 'api/users/'+ id;
        userService.deleteDetail(apiEndpoint)
        .then((response)=>{
            dispatch(deleteVendorsDetails());
            dispatch(userAction.getUser());
            history.push('/user');
        })
    };
}

export function changeVendorsList(user){
    return{
        type: "FETECHED_ALL_USER",
        user: user
    }
}

export function handleOnChangeProps(props, value){
    return{
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}

export function editUserDetails(user){
    return{
        type: "USER_DETAIL",
        _id: user._id,
        name: user.name,
        email: user.email,
        phone: user.phone,
        gender: user.gender
    }
}

export function updatedUserInfo(){
    return{
        type: "USER_UPDATED"
    }
}

export function createUserInfo(){
    return{
        type: "USER_CREATED_SUCCESSFULLY"
    }
}

export function deleteVendorsDetails(){
    return{
        type: "DELETED_VENDOR_DETAILS"
    }
}